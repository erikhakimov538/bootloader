/**
* @file 1986WSDDlg.h 
* @brief Application header file
*/
#pragma once
#include "ComPort.h"

/**
* A application class. Contains application functions for interacting with port and bootloader functions.
* Allows the user to clear the board memory, download, program, and verify hex files.
*/
class CMy1986WSDDlg  {
    public:
		/** 
        * \~english A public variable. Instance of the CCommPort class.
		* \~russian ��������� ����������. ��������� ������ CCommPort.
        */
		CCommPort com;
		/** 
        * \~english A public variable. Char buffer used to store data for write in port.
		* \~russian ��������� ����������. ����� ��������, ������������ ��� �������� ������ ��� ������ � ����.
        */
		char txdbuf[512];
		/** 
        * \~english A public variable. Char buffer used to store data read from port.
		* \~russian ��������� ����������. ����� ��������, ������������ ��� �������� ������, ��������� �� �����.
        */
		char rxdbuf[512];
		/** 
        * \~english A public variable. Char buffer used to store convert hex code.
		* \~russian ��������� ����������. ����� ��������, ������������ ��� �������� ������������������ ���� ��������������.
        */
		unsigned char bufcod[0x20000];
		/** 
        * \~english A public variable. Char buffer used to store convert bootloader code.
		* \~russian ��������� ����������. ����� ��������, ������������ ��� �������� ���� �������������� ����������.
        */
		unsigned char bufram[0x4000];
		/** 
        * \~english A public variable. Char buffer used to store string of hex code in convert function.
		* \~russian ��������� ����������. ����� ��������, ������������ ��� �������� ������ ������������������ ���� � ������� ��������������.
        */
		unsigned char buf_hexstr[530];
		/** 
        * \~english A public variable. Char buffer used to store data of hex code in convert function.
		* \~russian ��������� ����������. ����� ��������, ������������ ��� �������� ������ ������������������ ���� � ������� ��������������.
        */
		unsigned char buf_data_hex[256];

		unsigned char bl_hex;
		unsigned char btype_hex;
		unsigned short wadr_offs_hex;
		unsigned long dwadr_seg_hex;
		unsigned long dwadr_lineoffs_hex;
		unsigned long dwadrboot;
		/** 
        * \~english A public variable. Integer value store size of boot code.
		* \~russian ��������� ����������. ������������� �������� ������ ������ ������������ ����.
        */
		int ilboot;
		/** 
        * \~english A public variable. Integer value store size of hex code.
		* \~russian ��������� ����������. ������������� �������� ������ ������ ������������������ ����.
        */
		int ilcod;
    	std::string str;
		int bootcod, hexcod;
   		int rb, wb;
		/** 
        * \~english A public variable. Integer value used as a pointer to path for open a file.
		* \~russian ��������� ����������. ������������� ��������, ������������ � �������� ��������� ���� ��� �������� �����.
        */
    	int FileOpen, SecondFileOpen;
		/** 
        * \~english A public variable. Integer value used as a flag to enable/disable the function.
		* \~russian ��������� ����������. ������������� ��������, ������������ � �������� ����� ��� ���������/���������� �������.
        */
    	int m_erase, m_program, m_verify, m_load;

		struct {
			char filename[512];	/**< \~english Char value filename. \~russian ��� ����� � ������� char.*/ 
			char comname[16];	/**< char value comname. */
			int ibaud; 			/**< int value ibaud. */
		}InitParam;

        /**
        * \~english Initialization function. Checks and adjusts the configuration file, converts the hex codes to binary and sets the flags for the erase,
		* download, programming and verify functions.
		* @see 		Start()
		* @see 		GetInitParam()
        * @return 	Integer value that is 0(fail) or 1(success).
		* 
		* \~russian ������� �������������. ��������� � ������������ ���� ������������, ����������� ����������������� ���� � �������� � ������������� ����� ��� ��������,
		* ��������, ���������������� � �������� �������.
		* @see 		Start()
		* @see 		GetInitParam()
        * @return 	Integer value that is 0(fail) or 1(success).
        */
		virtual int OnInitDialog();

        /**
        * \~english Configuration file setting function.
		* @see 		Start()
		* @see 		OnInitDialog()
        * @return 	Integer value that is 0(fail) or 1(success).
		* 
		* \~russian ������� ��������� ����� ������������.
		* @see 		Start()
		* @see 		OnInitDialog()
        * @return 	Integer value that is 0(fail) or 1(success).
        */
		int GetInitParam(void);

        /**
        * \~english Erase plate memory by calling bootloader function, for futher load new hex code.
        * @see 		Verify()
		* @see 		Load()
		* @see 		Program()
        * @return 	Integer value that is 0(fail) or 1(success).
		* 
		* \~russian ������� ������ �����, ������ ������� ����������, ��� ���������� �������� ������ ������������������ ����.
		* @see 		Verify()
		* @see 		Load()
		* @see 		Program()
        * @return 	Integer value that is 0(fail) or 1(success).
        */
		int Erase(void);

        /**
        * \~english Programs a new hex code into the board's memory by calling bootloader function.
        * @see 		Verify()
		* @see 		Load()
        * @see 		Erase()
        * @return 	Integer value that is 0(fail) or 1(success).
		* 
		* \~russian ������������� ����� ����������������� ��� � ������ �����, ������� ������� ����������.
		* @see 		Verify()
		* @see 		Load()
        * @see 		Erase()
        * @return 	Integer value that is 0(fail) or 1(success).
        */
		int Program(void);

        /**
        * \~english Verify data on board by calling bootloader function.
		* @see 		Load()
        * @see 		Erase()
		* @see 		Program()
        * @return 	Integer value that is 0(fail) or 1(success).
		* 
		* \~russian ������������ ������ � ������ �����, ������ ������� ����������.
		* @see 		Load()
        * @see 		Erase()
		* @see 		Program()
        * @return 	Integer value that is 0(fail) or 1(success).
        */
		int Verify(void);

        /**
        * \~english The main function of the application from which all other functions of the code 
		* are called and executed. Checks the hex code and bootloader before loading, 
		* opens the port for work, performs synchronization, bootloader loading and signals
		* about errors in work (stopping work and closing the port).
		* @see 		Erase()
		* @see 		Program()
		* @see 		Verify()
		* @see 		Load()
		*
		* \~russian �������� ������� ����������, �� ������� ����������� ��� ��������� ������� ����.
		* ���������� � �����������. ��������� ����������������� ��� � ��������� ����� ���������,
		* ��������� ���� ��� ������, ���������� �������������, �������� ���������� � �������
		* �� ������� � ������ (��������� ������ � �������� �����).
		* @see 		Erase()
		* @see 		Program()
		* @see 		Verify()
		* @see 		Load()
        */
		void Start(void);

        /**
        * \~english Load a new hex code into the board's memory by calling bootloader function.
        * @see 		Erase()
		* @see 		Program()
		* @see 		Verify()
        * @return 	Integer value that is 0(fail) or 1(success).
		* 
		* \~russian ��������� ����� ����������������� ��� � ������ �����, ������ ������� ����������.
		* @see 		Erase()
		* @see 		Program()
		* @see 		Verify()
        * @return 	Integer value that is 0(fail) or 1(success).
        */
		int Load(void);

        /**
        * \~english Converts the hex code of the bootloader to a binary file for loading into the board's memory.
		* @see 		GetDataHex()
        * @see 		GetHexCod()
		* @see 		GetHexByte()
        * @return 	Integer value that is 0(fail) or 1(success).
		* 
		* \~russian ����������� ����������������� ��� ���������� � �������� ���� ��� �������� � ������ �����.
		* @see 		GetDataHex()
        * @see 		GetHexCod()
		* @see 		GetHexByte()
        * @return 	Integer value that is 0(fail) or 1(success).
        */
		int GetBootCod(void);

        /**
        * \~english Converts the hex code of the user program to a binary file for loading into the board's memory.
		* @see 		GetDataHex()
		* @see 		GetHexByte()
		* @see 		GetBootCod()
        * @return 	Integer value that is 0(fail) or 1(success).
		* 
		* \~russian ����������� ����������������� ��� ���������������� ��������� � �������� ���� ��� �������� � ������ �����.
		* @see 		GetDataHex()
		* @see 		GetHexByte()
		* @see 		GetBootCod()
        * @return 	Integer value that is 0(fail) or 1(success).
        */
		int GetHexCod(void);

        /**
        * \~english Converts an byte of data from hex to binary.
		* @param 	ibuf Integer value of index in array.
		* @see 		GetDataHex()
		* @see 		GetBootCod()
		* @see 		GetHexCod()
		* @return 	Converts resutl(bh+bl).
		*
		* \~russian ����������� ���� ������ �� ������������������ � ��������.
		* @param 	ibuf Integer value of index in array.
		* @see 		GetDataHex()
		* @see 		GetBootCod()
		* @see 		GetHexCod()
		* @return 	Converts resutl(bh+bl).
        */
		unsigned char GetHexByte(int ibuf);

        /**
        * \~english Converts an array of data from hex to binary.
		* @see 		GetHexByte()
		* @see 		GetBootCod()
		* @see 		GetHexCod()
		* @return 	Integer value that is 0(fail) or 1(success).
		*
		* \~russian ����������� ������ ������ �� ������������������ � ��������.
		* @see 		GetHexByte()
		* @see 		GetBootCod()
		* @see 		GetHexCod()
		* @return 	Integer value that is 0(fail) or 1(success).
        */
		int GetDataHex(void);
		
	private:
		char bufcurdir[512];
};
