/**
* @file 1986WSDDlg.cpp 
* @brief Cpp file with code of functions for aplication work. 
*/
#include <pthread.h>
#include "spdlog/spdlog.h"
#include "spdlog/async.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include <iostream>
#include <unistd.h>
#include <time.h>
#include <cstring>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <time.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdexcept>
#include <termios.h>
#include "1986WSDDlg.h"
#include "ComPort.h"
#include <chrono>
#include <thread>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

typedef std::string CString;

//������� �����������
/** 
* \~english Pointer to the record file for the logging system.
* \~russian ��������� �� ���� ������ ��� ������� �����������.
*/
auto appFileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/app-log.txt");
/** 
* \~english Logger pointer.
* \~russian ��������� ������.
*/
auto app_logger = std::make_shared<spdlog::logger>("App work", appFileSink);

int CMy1986WSDDlg::OnInitDialog()
{   
	//������� �������������
	app_logger->info("Start program...");
    printf ("Start program...\n");
    //�������� � ��������� ����� ������������
    if(!GetInitParam()) {   
		app_logger->error("Error open configuration file");
		return 0;  
	}
	//������ ����� ���������� (�������� � bufram)
	if(!GetBootCod()) {
		app_logger->error("Error open bootuartve4.HEX");
		return 0;  
	}
    app_logger->info("Init OK!");

	strcpy(InitParam.filename,"/home/erik/Projects/spi_led/build/uart_asm.hex");
	strcpy(InitParam.comname, "/dev/ttyUSB0");
    m_load = 0;
    m_erase = 1;
	m_program = 1;
	m_verify = 1;
    return 1;
}

//������� ��������� ����� ������������
int CMy1986WSDDlg::GetInitParam(void) {
	std::string strfn; 
    ssize_t i = readlink("/proc/self/exe", bufcurdir, 511);
	app_logger->info("Current directory: {}", bufcurdir);
    
	while(bufcurdir[i] !='/')
		i--;
	bufcurdir[i] = 0;
	strfn = "/1986VE4WSD.cfg";
	strfn = bufcurdir + strfn;
    const char* test = strfn.c_str();
    
    FileOpen = open(test, O_RDONLY);
	if(FileOpen == -1) {
		strcpy(InitParam.comname,"/dev/ttyUSB0");
		strcpy(InitParam.filename,"/home/erik/Projects/helper_k19/build/power_sys.hex");
        InitParam.ibaud = 3;
        SecondFileOpen = open(test, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

		if(SecondFileOpen == -1)
			return 0;

		write(SecondFileOpen, &InitParam, sizeof(InitParam));
		close(SecondFileOpen);
		return 1;
	}
	read(FileOpen, &InitParam,sizeof(InitParam));
	close(FileOpen);
	return 1;
}

//�������� ������� ����������
void CMy1986WSDDlg::Start(void) {
int i,j;
int f;
const char id_str[12]= "BOOTUARTVE4";

	if ((m_program == 1) || (m_verify == 1)) {
		//������ ���� ��������
        if(!GetHexCod()) {
			app_logger->error("Error open HEX-File!");
		    return;
        }
    }
		//�������� �����
    if(!com.Open(InitParam.comname, 9600, O_RDWR | O_NOCTTY | O_NDELAY | O_EXCL, 'N', 8, 1)) {
		app_logger->error("Error open COM port! COM port name: {}", InitParam.comname);
        return;
	}

	//�������� �������������� � ���� ��� ��������� �������� (0x0)
	app_logger->info("Synchronization... hCom: {}", com.hCom);
	txdbuf[0] = 0x0;
	for(i=0;i<512;i++) {
		com.WriteBlock(txdbuf, 1);
        }

    if(!com.ReadBlock(rxdbuf, 3)) {
		app_logger->error("First Readblock error!");
		for(i=0;i<512;i++) {
			com.WriteBlock(txdbuf, 1);	
            }
    
        if(!com.ReadBlock(rxdbuf,3)) {
			app_logger->error("Second Readblock error!");
			app_logger->error("Synchronization error!");
			com.Close();
			app_logger->info("COM close...");
			return;
		}
	}
	app_logger->info("Synchronization...OK!");

	// ��������� �������� ������
	const unsigned long b = 115200;
	txdbuf[0] = 'B';
	txdbuf[1] = (char)b;
	txdbuf[2] = (char)(b>>8);
	txdbuf[3] = (char)(b>>16);
	txdbuf[4] = 0x0;
	app_logger->info("txdbuf: {0:x}, {1:x}, {2:x}, {3:x}, {4:x}", uint32_t (txdbuf[0]&0x00ff), 
	uint32_t (txdbuf[1]&0x00ff), uint32_t (txdbuf[2]&0x00ff), uint32_t (txdbuf[3]&0x00ff), uint32_t (txdbuf[4]&0x00ff));
	com.WriteBlock(txdbuf, 5);
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    while(!com.ReadBlock(rxdbuf,1));
	app_logger->info("rxdbuf: hex: {0:x}", uint32_t (rxdbuf[0]&0x00ff));

	com.Close();
    if(!com.Open(InitParam.comname, 115200, O_RDWR | O_NOCTTY | O_NDELAY | O_EXCL, 'N', 8, 1)) {
		app_logger->error("Error open COM port! COM port name: {}", InitParam.comname);
        return;
	}

	//������ ����������� �������
	txdbuf[0] = 0xd;
	com.WriteBlock(txdbuf, 1);
	if (!com.ReadBlock(rxdbuf,3)){
		app_logger->error("Transfer error! Read operation error!");
		com.Close();
		return;
	}

	app_logger->info("rxdbuf: {0:x}, {1:x}, {2:x}", uint32_t (rxdbuf[0]&0x00ff), uint32_t (rxdbuf[1]&0x00ff), uint32_t (rxdbuf[2]&0x00ff));
	if((rxdbuf[0]!=0xd)||(rxdbuf[1]!=0xa)||(rxdbuf[2]!=0x3e)) {
		app_logger->error("Transfer error! Read return value error!");
		com.Close();
		return;
	    }

	//�������� ������� ����	
	app_logger->info("boot load...");
	txdbuf[0] = 'L';
	txdbuf[1] = dwadrboot & 0xff;
	txdbuf[2] = (dwadrboot>>8) & 0xff;
	txdbuf[3] = 0x10;
	txdbuf[4] = 0x20;
	txdbuf[5] = ilboot & 0xff;
	txdbuf[6] = (ilboot>>8) & 0xff;
	txdbuf[7] = 0;
	txdbuf[8] = 0;
	app_logger->info("txdbuf: {0:x}, {1:x}, {2:x}, {3:x}, {4:x}, {5:x}, {6:x}, {7:x}, {8:x}", uint32_t (txdbuf[0]&0x00ff), uint32_t (txdbuf[1]&0x00ff), 
	uint32_t (txdbuf[2]&0x00ff), uint32_t (txdbuf[3]&0x00ff), uint32_t (txdbuf[4]&0x00ff), uint32_t (txdbuf[5]&0x00ff), uint32_t (txdbuf[6]&0x00ff), 
	uint32_t (txdbuf[7]&0x00ff), uint32_t (txdbuf[8]&0x00ff));

	if(!com.WriteBlock(txdbuf, 9)) {
		app_logger->error("Transfer error! Write operation error!");
        com.Close();
		return;
	}

	if(!(com.ReadBlock(rxdbuf,1))) {
		app_logger->error("Transfer error! Read operation error!");
        com.Close();
		return;
	}

	if(rxdbuf[0]!='L') {
		app_logger->error("Transfer error! CMD_LOAD command error!");
        com.Close();
		return;
	    }

	//������ ����
	com.WriteBlock((char*)(bufram+dwadrboot), ilboot);
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	if(!(com.ReadBlock(rxdbuf,1))) {
		app_logger->error("Transfer error! Read operation error!");
        com.Close();
		return;
	}

	//�������� ��������� �������� �������
    if((rxdbuf[0]!='K')) {
        app_logger->error("Transfer error! Check REPLY_OK not return symbol 'K'");
		com.Close();
		return;
	    }

	//������ ������� ���� �� ������ ����������������
	for(i=0;i<(ilboot>>3);i++) {
		txdbuf[0] = 'Y';
		txdbuf[1] = (dwadrboot+8*i) & 0xff;
		txdbuf[2] = ((dwadrboot+8*i)>>8) & 0xff;
		txdbuf[3] = 0x10;
		txdbuf[4] = 0x20;
		txdbuf[5] = 8;
		txdbuf[6] = 0;
		txdbuf[7] = 0;
		txdbuf[8] = 0;
		com.WriteBlock(txdbuf, 9);
		f = true;
		
        if((com.ReadBlock(rxdbuf,10))&&(rxdbuf[0]=='Y')&&(rxdbuf[9]=='K')) {
			for(j=0;j<8;j++) {
				if(rxdbuf[j+1] != (char)bufram[dwadrboot+8*i+j])
					f= false;
			    }
		    }
		else
			f= false;
		if(!f) {
            app_logger->error("Transfer error");
			com.Close();
			return;
		}
	}

    //������ ��������� ���������� �� ����������
	txdbuf[0] = 'R';
	txdbuf[1] = bufram[dwadrboot+4] & 0xfe;
	txdbuf[2] = bufram[dwadrboot+5];
	txdbuf[3] = bufram[dwadrboot+6];
	txdbuf[4] = bufram[dwadrboot+7];
	com.WriteBlock(txdbuf, 5);
    if(!(com.ReadBlock(rxdbuf,1))||(rxdbuf[0]!='R')) {
        app_logger->error("Transfer error");			
		com.Close();
		return;
	    }

	txdbuf[0] = 'I';
	com.WriteBlock(txdbuf, 1);
	f = true;

	if(com.ReadBlock(rxdbuf,11)) {
		for(j=0;j<11;j++) {
				if(rxdbuf[j] != id_str[j])
					f= false;
			    }
	    }
	else
		f = false;

	if(f == false) {   
		app_logger->error("Loader identification error!");
		com.Close();
		return;
	}
	app_logger->info("boot load...OK!");
    
	if(m_load == 1) {
		Load();
		return;
	}
	if(m_erase == 1)
		if(!Erase())
			return;
	if(m_program == 1)
		if(!Program())
			return;
	if(m_verify == 1)
		if(!Verify())
			return;
	com.Close();
}

//������ ���� ����������
int CMy1986WSDDlg::GetBootCod(void) {
int i,nb;
CString strfn;
char chd;
unsigned long	dwadr;
dwadr_seg_hex = 0;
dwadr_lineoffs_hex = 0;

	for(i=0;i<sizeof(bufram);i++) {
		bufram[i] = 0xff;
	}

	strfn = "/bootuartve4.hex";
	strfn = bufcurdir + strfn;
    const char* test = strfn.c_str();
    if(!(bootcod = open(test, O_RDONLY, NULL))) {
		app_logger->error("Open error!");
		return 0;
        }

	nb = 1;
	while(nb == 1) {
		i = 0;
		do {
			nb = read(bootcod, &chd, 1);
            buf_hexstr[i] = chd;
			i++;
		}

		while(chd!='\n');
		if(nb != 1) {
            close(bootcod);
			for(i=0;i<sizeof(bufram);i++) {
				if(bufram[i] != 0xff)
					break;
			}
			dwadrboot = i;
			for(i=(sizeof(bufram)-1);i>=0;i--) {
				if(bufram[i] != 0xff)
					break;
			}
			ilboot = (i+8 - dwadrboot) & 0xfffffff8;
			return 1;
		}

		if(!GetDataHex()) {
            close(bootcod);
			return 0;
		}

		if(btype_hex == 0) {
			dwadr = dwadr_lineoffs_hex + dwadr_seg_hex + wadr_offs_hex;
			if((dwadr<0x20000000)||((dwadr+bl_hex)>0x20003fff)) {
                close(bootcod);
				return 0;
			}
			dwadr -= 0x20000000;
			for(i=0;i<bl_hex;i++)
				bufram[dwadr+i] = buf_data_hex[i];
		}
	}
	return 1;
}

//������ ���� ��������
int CMy1986WSDDlg::GetHexCod(void) {
int i,nb;
CString strfn;
char chd;
unsigned long	dwadr;
dwadr_seg_hex = 0;
dwadr_lineoffs_hex = 0;

	for(i=0;i<sizeof(bufcod);i++) {
		bufcod[i] = 0xff;
	}
	hexcod = open(InitParam.filename, O_RDONLY);
    if(hexcod == -1)
        return 0;
	nb = 1;
	while(nb == 1) {
		i = 0;
		do {
            nb = read(hexcod,&chd,1);
			buf_hexstr[i] = chd;
			i++;
		}
		while(chd!='\n');
		if(nb != 1) {
            close(hexcod);
			for(i=(sizeof(bufcod)-1);i>=0;i--) {
				if(bufcod[i] != 0xff)
					break;
			}
			i = ((i + 0x100) & 0xffffff00);
			ilcod = i;
			return 1;
		}
		if(!GetDataHex()) {
            close(hexcod);
			return 0;
		}
		if(btype_hex == 0) {
			dwadr = dwadr_lineoffs_hex + dwadr_seg_hex + wadr_offs_hex;
			if((dwadr+bl_hex)> 0x00020000) {
				close(hexcod);
                return 0;
			}
			for(i=0;i<bl_hex;i++)
				bufcod[dwadr+i] = buf_data_hex[i];
		}
	}
	return 0;	
}

unsigned char CMy1986WSDDlg::GetHexByte(int ibuf) {
unsigned char bh,bl;

	bh = buf_hexstr[ibuf] - 0x30;
	if (bh>9)
		bh -= 7;
	bh <<= 4;
	bl = buf_hexstr[ibuf+1] - 0x30;
	if (bl>9)
		bl -= 7;
	return bh+bl;
}

int CMy1986WSDDlg::GetDataHex(void) {
int i;
unsigned char ks;

	if(buf_hexstr[0] != 0x3a)
		return	0;
	ks = 0;
	bl_hex = GetHexByte(1);
	wadr_offs_hex = ((unsigned short)GetHexByte(3)<<8)+GetHexByte(5);
	btype_hex = GetHexByte(7);
	ks = bl_hex + btype_hex + (wadr_offs_hex>>8) + wadr_offs_hex;
	for(i=0;i<bl_hex+1;i++) {
		buf_data_hex[i] = GetHexByte(2*i+9);
		ks += buf_data_hex[i];
	}

	if(ks!=0)
		return 0;
	if(btype_hex == 2)
		dwadr_seg_hex =	(((unsigned short)GetHexByte(9)<<8)+GetHexByte(11))<<4;
	if(btype_hex == 4)
		dwadr_lineoffs_hex = (((unsigned short)GetHexByte(9)<<8)+GetHexByte(11))<<16;
	return 1;
}

//������ ������� ������ ����� ��������� ����������
int CMy1986WSDDlg::Erase(void) {
unsigned long adr,data;	
app_logger->info("Full chip erase...");

    txdbuf[0] = 'E';
	wb = com.WriteBlock(txdbuf, 1);
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    rb = com.ReadBlock(rxdbuf,9);
    if	((rb == -1)||(rxdbuf[0]!='E')) {
		app_logger->error("Transfer error");
		com.Close();
		return 0;
	}

	app_logger->info("rxdbuf: {0:x}, {1:x}, {2:x}, {3:x}, {4:x}, {5:x}, {6:x}, {7:x}, {8:x}", uint32_t (rxdbuf[0]&0x00ff), uint32_t (rxdbuf[1]&0x00ff), 
	uint32_t (rxdbuf[2]&0x00ff), uint32_t (rxdbuf[3]&0x00ff), uint32_t (rxdbuf[4]&0x00ff), uint32_t (rxdbuf[5]&0x00ff), uint32_t (rxdbuf[6]&0x00ff), 
	uint32_t (rxdbuf[7]&0x00ff), uint32_t (rxdbuf[8]&0x00ff));
	
	adr = (uint32_t)rxdbuf[1] | (((uint32_t)rxdbuf[2])<<8) | (((uint32_t)rxdbuf[3])<<16) | (((uint32_t)rxdbuf[4])<<24);
	data = (uint32_t)rxdbuf[5] | (((uint32_t)rxdbuf[6])<<8) | (((uint32_t)rxdbuf[7])<<16) | (((uint32_t)rxdbuf[8])<<24);

	app_logger->info("adr, data: {0:x}, {1:x}", adr, data);
	if((adr == 0x000020000)&&(data == 0xffffffff)) {
		app_logger->info("Full chip erase done!");
        return 1;
	}
	app_logger->error("Full chip erase failed!");
    com.Close();
	return 0;
}

//������ ���������������� ������ ����� ��������� ����������
int CMy1986WSDDlg::Program(void) {
int i,j,k;
unsigned char ks;

	app_logger->info("Program...");
    txdbuf[0] = 'A';
	txdbuf[1] = 0x00;
	txdbuf[2] = 0x00;
	txdbuf[3] = 0x00;
	txdbuf[4] = 0x00;
	wb = com.WriteBlock(txdbuf, 5);
    rb = com.ReadBlock(rxdbuf,1);
    if((rb == -1)||(rxdbuf[0]!=0x00)) {	
		app_logger->error("Transfer error");
        com.Close();
		return 0;
	}

	txdbuf[0] = 'P';
	// printf("ilcod %d \n", ilcod);
	for(i=0;i<((ilcod>>8)+1);i++) {
		wb = com.WriteBlock(txdbuf, 1);
		wb = com.WriteBlock((char *)&bufcod[i<<8], 256);
		ks =0;
		for(j=0;j<256;j++) {
			ks += bufcod[j+(i<<8)];
			// for(k = 0; k < 256; k++) {
			// 	printf("%02x ", bufcod[k+(i<<8)]);
			// 	if(k%16 == 0) {
			// 		printf("\n");
			// 	}
			// }
		}

        rb = com.ReadBlock(rxdbuf,1);
        if((rb == -1)||((unsigned char)rxdbuf[0]!=ks)) {
			app_logger->error("Transfer error");
            com.Close();
			return 0;
		}
	}
	app_logger->info("Program done!");
	return 1;
}

//������ ����������� ������ ����� ��������� ����������
int CMy1986WSDDlg::Verify(void) {
int i,j,k;

	app_logger->info("Verify...");
	txdbuf[0] = 'A';
	txdbuf[1] = 0x00;
	txdbuf[2] = 0x00;
	txdbuf[3] = 0x00;
	txdbuf[4] = 0x00;
	wb = com.WriteBlock(txdbuf, 5);
    rb = com.ReadBlock(rxdbuf,1);
    if((rb == -1)||(rxdbuf[0]!=0x00)) {
		app_logger->error("Transfer error");
		com.Close();
		return 0;
	}
	txdbuf[0] = 'V';
	// printf("ilcod %d \n", ilcod);
	for(i=0;i<(ilcod>>8);i++) {
		for(j=0;j<32;j++) {
			wb = com.WriteBlock(txdbuf, 1);
            rb = com.ReadBlock(rxdbuf,8);
            if(rb == -1) {
				app_logger->error("Transfer error");
                com.Close();
				return 0;
			}
			// for(k=0;k<8;k++) {
			// 	if((unsigned char)rxdbuf[k] != bufcod[k+(j<<3)+(i<<8)]) {
			// 		app_logger->error("Verify failed!");
			// 		com.Close();
			// 		return 0;
			// 	}
			// }
		}
	}
	app_logger->info("Verify done!");
	return 1;	
}

//������ �������� ����� ��������� ����������
int CMy1986WSDDlg::Load(void) {
int i,j,k;
app_logger->info("Load...");

	txdbuf[0] = 'A';
	txdbuf[1] = 0x00;
	txdbuf[2] = 0x00;
	txdbuf[3] = 0x00;
	txdbuf[4] = 0x00;
	wb = com.WriteBlock(txdbuf, 5);
    rb = com.ReadBlock(rxdbuf,1);
    if((rb == -1)||(rxdbuf[0]!=0x00)) {
		app_logger->error("Transfer error");
		com.Close();
		return 0;
	}
	txdbuf[0] = 'V';
	for(i=0;i<512;i++) {
		for(j=0;j<32;j++) {
			wb = com.WriteBlock(txdbuf, 1);
            rb = com.ReadBlock(rxdbuf,8);
            if(rb == -1) {
				app_logger->error("Transfer error");
				com.Close();
				return 0;
			}
			for(k=0;k<8;k++)
				bufcod[k+(j<<3)+(i<<8)] = (unsigned char)rxdbuf[k];
		}
	}

	CString strfn;
	strfn = "\\1986VE4.bin";
	strfn = bufcurdir + strfn;
    const char *strfn_c = strfn.c_str();
    FileOpen = open(strfn_c, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    write(FileOpen, &InitParam, sizeof(InitParam));
	close(FileOpen);
	app_logger->info("Load done!");
	return 1;	
}

/** 
* \~english The main function of the code. The work of the program starts from it, all other functions are called and returned.
* \~russian �������� ������� ����. � ��� �������� ������ ���������, ���������� � ������������ ��� ��������� �������.
*/
int main() {
    std::string NextStep;
    CMy1986WSDDlg app;

    while(true) {
        if (app.OnInitDialog() == 0) {
			app_logger->error("Initialization error, try run again...");
            break;
        } else {
            app.Start();
        }
		app_logger->info("Shutdown...\n");
		printf ("Shutdown...\n");
        break;
    }
}

