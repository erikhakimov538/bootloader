# Short description # {#mainpage}
This project is a port of the Linux bootloader program for the helper_k19 board from Milandr. This program is built and run through the Linux system console or through the IDE console. The program receives a user-defined hex file as input and loads it into the board's memory (first converting the hex into binary and clearing the board's memory). It is important that the bootloader hex file is in the same directory as the project, the file with the user code can be located in any directory to which you can set the path. The settings of the bootloader program are carried out by replacing values in the project code and rebuilding it through the console using the ```make``` command (the project contains the build script ```Makefile```). Also this project have log system(spdlog) for the convenience of working with the program code. All log files you can find in folder ```logs``` (app-log.txt - contains a log of application functions in the file 1986WSDDlg.cpp, com-log.txt - contains the log of the COM port functions from the file ComPort.cpp).

## Build, run and setting instructions for Linux system
### Build
This project have automatic build script ```Makefile```. For build this project, you need just write one command in console (for clean make directory and some related files you need command ```make clean```).

1. Go to project path:
    ```cd <project_path>```

2. Copy this repo to your computer:    ```git clone git@gitlab.com:erikhakimov538/bootloader.git``` or ```git clone https://gitlab.com/erikhakimov538/bootloader.git```

3. Go to build folder:
    ```cd build```
    (If the folder does not exist, it must be created: ```mkdir build```)

4. Open project in your IDE and write to console (you can also use system console): ```make```

After all actions project start generate object file ```bootloader```.

### Run and setting
After successfully create object file ```bootloader``` you need connect your board to computer and check this connection in console by command: ```sudo dmesg``` (if you have error, you need install dmesg ```sudo apt install dmesg``` ).
Default return value by dmesg ```/dev/ttyUSB0```, but if you have another one, you can change it in code on ```str 63```, it looks like: ```strcpy(InitParam.comname, "<your_port>")```.
Also you need change the path of hex file that you want upload to board (```str 62```): ```strcpy(InitParam.filename,"<your_path>")```
Some more settings in code, that you can change:
-```m_erase = 1``` or ```0``` - in default board memory erased by bootloader
-```m_program = 1``` or ```0```- in default bootloader program you hex file to board
-```m_verify = 1``` or ```0```- in default bootloader verify code on board

After all settings you need rebuild project by command ```make``` for apply changes.
And run object file in console by ```./bootloader```.  


# ������� ��������  <!--- # {#mainpage} -->
���� ������ �������� ������ ���������-���������� �� ������� Linux ��� ����� helper_k19 �� Milandr. ��� ��������� ��������� � ����������� ����� ��������� ������� Linux ��� ����� ������� IDE. ��������� �������� �� ���� ���������������� ����������������� ���� � ��������� ��� � ������ ����� (������� ���������� ����������������� ���� � �������� � ������ ������ �����). �����, ����� ����������������� ���� ���������� ��������� � ��� �� ����������, ��� � ������, ���� � ����� ������������ ����� ������������� � ����� ����������, � ������� ����� ������ ����. ��������� ���������-���������� �������������� ������� �������� � ���� ������� � ��� ����������� ����� ������� � ������� ������� ```make``` (������ �������� ������ ������ ```Makefile```). ��� �� � ���� ������� ���� ���-�������(spdlog) ��� �������� ������ � ����������� �����. ��� ���-����� �� ������ ����� � ����� ```logs```(app-log.txt - �������� ��� ������� ���������� � ����� 1986WSDDlg.cpp, com-log.txt - �������� ��� ������� COM-����� �� ���� ComPort.cpp).

## ���������� �� ������, ��������� � ������� ��� ������ ������
### ������
���� ������ ����� ������ �������������� ������ Makefile. ��� ������ ����� ������� ��� ����� ����� ���� �������� ���� ������� � ������� (��� ������ ```make```, ��� ������� �������� make � ��������� ��������� ������ ```make clean```).

1. ��������� � ���� � �������:
     ```cd <����_�������>```

2. ���������� ���� ����������� �� ���� ���������: ```git clone git@gitlab.com:erikhakimov538/bootloader.git``` ��� ```git clone https://gitlab.com/erikhakimov538/bootloader.git```

3. ��������� � ����� ������:
     ```������ �������-�����```
     (���� ����� �� ����������, �� ���������� �������: ```mkdir build```)

4. �������� ������ � ����� ����� IDE � �������� � ������� (�� ����� ������ ������������ ��������� �������): ```make```

����� ���� �������� ������ ������ ������������ ��������� ���� ```bootloader```.

### ��������� � ������
����� ��������� �������� ���������� ����� ```bootloader``` ��� ���������� ���������� ���� ����� � ���������� � ��������� ��� ���������� � ������� ��������:             ```sudo dmesg``` (���� � ��� ���� ������, ��� ����� ���������� dmesg ```sudo apt install dmesg``` ).
�������� �� ���������, ������������ dmesg ```/dev/ttyUSB0```, �� ���� � ��� ���� ������, �� ������ �������� ��� � ���� �� ```str 63```, ��� �������� ���: ```strcpy(InitParam. comname, "<���_����>");```.
����� ��� ����� �������� ���� � ������������������ �����, ������� �� ������ ��������� �� ����� (```str 62```):
```strcpy(InitParam.filename,"<���_����>");```
��� ��������� �������� � ����, ������� �� ������ ��������:
-```m_erase = 1``` ��� ```0``` - �� ��������� ������ ����� ��������� �����������
-```m_program = 1``` ��� ```0```- � ��������� ���������� �� ��������� �� ����������������� ���� ��� �����
-```m_verify = 1``` ��� ```0```- � ���������� �� ��������� ��������� ��� �� �����

����� ���� �������� ��� ���������� ����������� ������ �������� make ��� ���������� ���������.
� ��������� ��������� ���� � ������� � ������� ```./bootloader```. 